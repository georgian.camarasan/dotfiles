" A command-line fuzzy finder
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }

" super simple vim plugin to show the list of buffers in the command bar
Plug 'bling/vim-bufferline'

" Fuzzy file, buffer, mru, tag, etc finder
Plug 'ctrlpvim/ctrlp.vim'

" BufExplorer
" Plug 'jlanzarotta/bufexplorer'

" Vim plugin which asks for the right file to open
Plug 'EinfachToll/DidYouMean'

