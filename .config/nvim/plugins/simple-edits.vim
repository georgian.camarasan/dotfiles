" Vim alignment
Plug 'junegunn/vim-easy-align'

" quoting/paranthesizing made simple
Plug 'tpope/vim-surround'

" Vim plugin for intensely orgasmic commenting
Plug 'scrooloose/nerdcommenter'

" Vim motion on speed!
Plug 'easymotion/vim-easymotion'

" Maintains a history of previous yanks, changes and deletes
Plug 'vim-scripts/YankRing.vim'

" A Vim plugin that always highlights the enclosing html/xml tags
Plug 'Valloric/MatchTagAlways'

" A vim plugin that simplifies the transition between multiline and single-line code
Plug 'AndrewRadev/splitjoin.vim'

" Better whitespace highlighting for Vim
Plug 'ntpeters/vim-better-whitespace'

" Show 'Match 123 of 456 /search term/' in Vim searches.
Plug 'henrik/vim-indexed-search'

" Vim mapping for sorting a range of text
Plug 'christoomey/vim-sort-motion'

