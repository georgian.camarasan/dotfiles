" Defaults everyone can agree on
Plug 'tpope/vim-sensible'

" continuously updated session files
Plug 'tpope/vim-obsession'

" Miscellaneous auto-load Vim scripts
Plug 'xolox/vim-misc'

" Vim plugin for insert mode completion of words in adjacent tmux panes
Plug 'wellle/tmux-complete.vim'

" Seamless navigation between tmux panes and vim splits <Paste>
Plug 'christoomey/vim-tmux-navigator'

" Calendar from the vim site
Plug '~/code/vim-plugins/calendar.vim'

" Personal Wiki for Vim http://vimwiki.github.io/
Plug 'vimwiki/vimwiki', { 'branch': 'dev' }

