" A Git wrapper so awesome, it should be illegal<Paste>
Plug 'tpope/vim-fugitive'

" A Vim plugin for more pleasant editing on commit messages
Plug 'rhysd/committia.vim'

" A plugin of NERDTree showing git status
" Plug 'Xuyuanp/nerdtree-git-plugin'
